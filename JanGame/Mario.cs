﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    /// <summary>
    /// The main player object.
    /// </summary>
    class Mario : Sprite
    {
        /// <summary>
        /// Is the object currently on the ground.
        /// </summary>
        bool IsGrounded = false;

        /// <summary>
        /// Is the object actively jumping (not just simply falling).
        /// </summary>
        bool IsJumping = false;

        /// <summary>
        /// Does the player need to reach a reseting point before they can jump again?
        /// </summary>
        bool IsJumpResetRequired = false;

        /// <summary>
        /// Is the object sprinting (rather than walking).
        /// </summary>
        bool IsSprinting = false;

        /// <summary>
        /// Is the player currently dashing.
        /// </summary>
        bool IsDashing = false;

        /// <summary>
        /// Does the player need to reach a resting point before they can dash again?
        /// </summary>
        bool IsDashResetRequired = false;

        /// <summary>
        /// Is the jump button being held right now.
        /// </summary>
        bool IsJumpPressed = false;

        /// <summary>
        /// Is a direction button being pressed right now.
        /// </summary>
        bool IsDirPressed = false;

        /// <summary>
        /// How fast is the object moving.
        /// </summary>
        Vector2 CurVel;

        /// <summary>
        /// How long has the jump button been held?
        /// </summary>
        float CurrentJumpHoldTime = 0.0f;

        /// <summary>
        /// The number of ticks that have passed since the player last touched the ground.
        /// </summary>
        float TickSinceGrounded = 0.0f;

        /// <summary>
        /// The amount of time that has passed since the player started dashing.
        /// </summary>
        float TicksSinceDashStarted = 0f;

        /// <summary>
        /// Is the player currently floating dead in lava.
        /// </summary>
        bool IsInLava = false;

        /// <summary>
        /// How many ticks have passed since the player entered the lava.
        /// </summary>
        float TicksSinceLava = 0;

        /// <summary>
        /// How long does the player need to be in lava before we restart the level.
        /// </summary>
        float TicksForLavaRespawn = 120;

        /// <summary>
        /// Set to true if you want this character to fall hard into the ground.
        /// </summary>
        public bool FallingHard = false;

        /// <summary>
        /// Did the player fall hard into the ground, and now needs to recover?
        /// </summary>
        bool FellHard = false;

        /// <summary>
        /// How many ticks have elapsed since the player slammed hard into the ground?
        /// </summary>
        int TicksSinceFellHard = 0;

        /// <summary>
        /// How many ticks must elapse after falling hard, before the player can get up.
        /// </summary>
        int TicksToStayFellHard = 60;

        bool IsGoalHit = false;

        int TicksSinceHitGoal = 0;
        Vector2 PostionWhenHitGoal;

        GoalPoint GoalHit;

        SoundEffect FootStepSound;
        SoundEffectInstance FootStepSoundInst;
        SoundEffect DashSound;
        SoundEffect BounceSound;
        SoundEffect DeathSound;
        SoundEffect GoalSound;

        public bool CameraFollowOn = true;

        //bool StartBounce = false;

        // ANIMATION NAMES
        //

        public const string Anim_Walk = "Walk";
        public const string Anim_Sprint = "Sprint";
        public const string Anim_Idle = "Idle";
        public const string Anim_Jump = "Jump";
        public const string Anim_Fall = "Fall";
        public const string Anim_LandHard = "LandHard";
        public const string Anim_TryGetUp = "TryGetUp";

        // TUNABLES
        //

        const float JumpLaunchVel = 0.8f;
        const float Gravity = 0.11f;
        const float WalkAccel = 0.1f;
        const float MaxWalkSpeed = 0.75f;
        const float MaxRunSpeed = 1.0f;
        const float MaxDashSpeed = 4.0f;
        const float DashTickLength = 10.0f;
        const float DashBounceJumpMod = 4.0f;
        const float MaxJumpHoldTime = 15;
        const float UngroundedJumpTickCount = 7;
        const int TicksToTransitionAtGoal = 60;

        const float SoundVolume = 0.25f;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="Game"></param>
        public Mario(Game1 Game) : base(Game)
        {
            FootStepSound = Game.Content.Load<SoundEffect>("footstep");
            FootStepSoundInst = FootStepSound.CreateInstance();
            DashSound = Game.Content.Load<SoundEffect>("dash");
            BounceSound = Game.Content.Load<SoundEffect>("bounce");
            DeathSound = Game.Content.Load<SoundEffect>("death");
            GoalSound = Game.Content.Load<SoundEffect>("snd_goal");

            // Entering the lava caused the screen to fade white, so turn it back.
            PostProcessScalarTween PP = new PostProcessScalarTween(Game, 1, 0, 15, "LavaColorScale",
                Game.PostProcessEffect);
            Game.SpritesToAdd.Add(PP);

            PostProcessVectorTween PPV = new PostProcessVectorTween(Game, new Vector4(1, 1, 1, 1),
                new Vector4(0, 0, 0, 0), TicksForLavaRespawn * 0.75f, "Tint", SpriteEffect);
            Game.SpritesToAdd.Add(PPV);

        }

        /// <summary>
        /// Called when the player falls into deadly lava!
        /// </summary>
        public void OnEnterLava()
        {
            if (!IsInLava)
            {
                TicksSinceLava = 0;
                IsInLava = true;

                SetNextAnim(Anim_Fall);

                // Fade the screen white.
                float InstantFlashTicks = 15;

                Action Act = () => {
                    PostProcessScalarTween Tween = new PostProcessScalarTween(Game, 0, 1, 
                        TicksForLavaRespawn * 0.75f - InstantFlashTicks, "LavaColorScale", Game.PostProcessEffect);
                    Game.SpritesToAdd.Add(Tween);
                };

                PostProcessScalarTween PP = new PostProcessScalarTween(Game, 1, 0, 
                    InstantFlashTicks, "LavaColorScale", Game.PostProcessEffect, Act);
                Game.SpritesToAdd.Add(PP);


                Vector3 LavaColor = new Vector3(0.75f, 0.2f, 0.25f);
                // Fade the player white.
                PostProcessVectorTween PPV = new PostProcessVectorTween(Game, new Vector4(LavaColor, 0), 
                    new Vector4(LavaColor, 1), TicksForLavaRespawn * 0.25f, "Tint", SpriteEffect);
                Game.SpritesToAdd.Add(PPV);

                DeathSound.Play(SoundVolume, 0, 0);
            }
        }

        public void OnHitGoal(GoalPoint Goal)
        {
            if (!IsGoalHit && !IsInLava)
            {
                IsGoalHit = true;
                GoalHit = Goal;
                PostionWhenHitGoal = Position;

                Vector3 Tint = new Vector3(0, 0, 0);
                // Fade the player white.
                PostProcessVectorTween PPV = new PostProcessVectorTween(Game, 
                    new Vector4(Tint, 0),
                    new Vector4(Tint, 1), 
                    TicksToTransitionAtGoal * 0.25f, 
                    "Tint", 
                    SpriteEffect);
                Game.SpritesToAdd.Add(PPV);

                GoalSound.Play(SoundVolume, 0, 0);
            }
        }

        public override void Update()
        {
            //if (Keyboard.GetState().IsKeyDown(Keys.F3))
            //{
            //    DebugPos = Vector2.Zero;
            //}

#if DEBUG
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                Position.Y -= 10;
            }
#endif

            // Once we hit lava, circumvent most of the logic.
            // NOTE: This causes a bug where the player can go through walls but might not be an issue
            //       if we do a disovle effect on the player.
            if (IsInLava)
            {
                TicksSinceLava++;

                // Slowly bring the player to a halt in the X.
                CurVel.X *= 0.8f;
                Position.X += CurVel.X;

                // Slowly let the player float down, as if in water.
                Position.Y += 0.1f;

                // Check if enough time has passed for the player to respawn.
                if (TicksSinceLava > TicksForLavaRespawn)
                {
                    //IsInLava = false;
                    Game.Respawn();

                    //// Entering the lava caused the screen to fade white, so turn it back.
                    //PostProcessScalarTween PP = new PostProcessScalarTween(Game, 1, 0, 15, "LavaColorScale", 
                    //    Game.PostProcessEffect);
                    //Game.SpritesToAdd.Add(PP);

                    //PostProcessVectorTween PPV = new PostProcessVectorTween(Game, new Vector4(1,1,1,1), 
                    //    new Vector4(0,0,0,0), TicksForLavaRespawn * 0.75f, "Tint", SpriteEffect);
                    //Game.SpritesToAdd.Add(PPV);
                }

                // Need to update the player to ensure the bounds and animations get updated.
                base.Update();
// Early return.
                return;
            }

            if (FellHard)
            {
                //RotationRadians = 0;
                if (InputManager.pInstance.CheckAction(InputManager.InputActions.LA_LEFT) 
                    || InputManager.pInstance.CheckAction(InputManager.InputActions.LA_RIGHT)
                    || InputManager.pInstance.CheckAction(InputManager.InputActions.LA_UP)
                    || InputManager.pInstance.CheckAction(InputManager.InputActions.X)
                    || InputManager.pInstance.CheckAction(InputManager.InputActions.A))
                {
                    TicksSinceFellHard++;
                    SetNextAnim(Anim_TryGetUp);
                    
                    if (TicksSinceFellHard >= TicksToStayFellHard)
                    {
                        FellHard = false;
                    }
                }
                else
                {
                    TicksSinceFellHard = 0;
                    SetNextAnim(Anim_LandHard);
                }
                
                base.Update();

                return;
            }

            if (IsGoalHit)
            {
                SetNextAnim(Anim_Fall);
                TicksSinceHitGoal++;

                Vector2 GoalBottom = GoalHit.Bounds.pCenterBottom;
                
                Position = Vector2.SmoothStep(PostionWhenHitGoal, GoalBottom, (float)TicksSinceHitGoal / (float)TicksToTransitionAtGoal);
                Scale = MathHelper.SmoothStep(1, 0.8f, (float)TicksSinceHitGoal / (float)TicksToTransitionAtGoal);

                if (TicksSinceHitGoal >= TicksToTransitionAtGoal)
                {
                    GoalHit.OnTransitionToGoalResult();
                }

                //TicksSinceHitGoal++;
                //float MoveSpeed = 0.1f;
                //Vector2 GoalBottom = GoalHit.Bounds.pCenterBottom;
                //if ((GoalBottom - Position).Length() >= MoveSpeed)
                //{
                //    Vector2 Dir = GoalBottom - Position;
                //    Dir.Normalize();

                //    //Position += Dir * MoveSpeed;

                //    Position = Vector2.SmoothStep(PostionWhenHitGoal, GoalBottom, TicksSinceHitGoal / TicksToTransitionAtGoal);
                //}
                //else
                //{
                //    if (TicksSinceHitGoal >= TicksToTransitionAtGoal)
                //    {
                //        GoalHit.OnTransitionToGoalResult();
                //    }
                //}

                base.Update();

                return;
            }

            //if (FallingHard)
            //{
            //    RotationRadians += MathHelper.ToRadians(1);
            //}

            // MOVEMENT ROUNTINES
            //

            // Assume that we are not spriting at the start of every update.
            IsSprinting = false;

            // Start with the max WALK speed, and potentially change if they are trying to sprint.
            float CurrentMaxMoveSpeedX = MaxWalkSpeed;

            if (InputManager.pInstance.CheckAction(InputManager.InputActions.A, true))
            {
                if (!IsDashResetRequired && !FallingHard)
                {
                    TicksSinceDashStarted = 0;

                    QuickEffect TeleportFlash = new QuickEffect(Game)
                    {
                        SpriteSheet = Game.Content.Load<Texture2D>("explosion2"),
                        SpriteCell = new Point(16, 16),
                        Origin = new Vector2(16 / 2, 12),
                    };
                    TeleportFlash.Animations.Add("Idle", new Sprite.Anim(0, 3, 3, true));
                    TeleportFlash.SetNextAnim("Idle");
                    TeleportFlash.Position = Position;

                    Game.SpritesToAdd.Add(TeleportFlash);

                    IsDashing = true;
                    IsDashResetRequired = true;

                    if ((SpriteFX & SpriteEffects.FlipHorizontally) == SpriteEffects.None)
                    {
                        CurVel.X = MaxDashSpeed;
                    }
                    else
                    {
                        CurVel.X = -MaxDashSpeed;
                    }
                    CurVel.Y = 0;
                    DashSound.Play(SoundVolume, 0, 0);
                }
            }

            // Check for SPRINT.
            if (!InputManager.pInstance.CheckAction(InputManager.InputActions.X))
            {
                // Increase the max movement speed.
                CurrentMaxMoveSpeedX = MaxRunSpeed;

                // This object is now sprinting.
                IsSprinting = true;
            }

            // Keep track of the object's position at the start of the frame.
            Vector2 startPos = Position;

            if (IsDashing)
            {
                TicksSinceDashStarted++;

                if (TicksSinceDashStarted >= DashTickLength)
                {
                    IsDashing = false;
                }

                //DoJumpRoutine(StartBounce);
            }
            else
            {
                // Check for WALK
                DoWalkRoutine(InputManager.pInstance.CheckAction(InputManager.InputActions.LA_LEFT), InputManager.pInstance.CheckAction(InputManager.InputActions.LA_RIGHT));

                // Clamp by the current max speed.
                CurVel.X = MathHelper.Clamp(CurVel.X, -CurrentMaxMoveSpeedX, CurrentMaxMoveSpeedX);

                // Check for JUMP
                //DoJumpRoutine(InputManager.pInstance.CheckAction(InputManager.InputActions.B));
            }

            //MoveWithPhysics();

            // We only check collision in the direction we are moving, so see which direction we are moving.
            bool isMovingRight = CurVel.X > 0;
            bool isMovingLeft = CurVel.X < 0;

            // COLLISION ROUNTINES
            //

            if (!IsDashing)
            {
                CurVel.Y = Math.Min(CurVel.Y, 4f);
                Position.Y += CurVel.Y;
            }

            // Going Up
            CollideUp();

            // Falling
            bool isOnRamp = CollideDown();


            Position.X += CurVel.X;

            // If we are on a ramp, colliding with right/left walls could cause the object to get
            // stuck on the solid blocks UNDER the ramp. To avoid this we don't check for left/right
            // collision when on ramps. This has a knock-on bug that you can walk partially into a wall
            // if you have a ramp leading directly into it.
            if (!isOnRamp)
            {
                // Walk Right
                bool StartBounce = CollideRight(isMovingRight, startPos);

                // Walk Left
                StartBounce |= CollideLeft(isMovingLeft, startPos);

                if (StartBounce && IsDashing)
                {
                    DoBounce();
                }
            }

            // Show the jump sprite as needed.
            if (!IsGrounded || IsDashing)
            {
                if (CurVel.Y < 0)
                {
                    SetNextAnim(Anim_Jump);
                }
                else
                {
                    SetNextAnim(Anim_Fall);
                }
            }

            if (!IsGrounded)
            {
                TickSinceGrounded++;
            }

            if (NextAnim == Anim_Sprint)
            {
                if (FootStepSoundInst.State == SoundState.Stopped)
                {
                    FootStepSoundInst.Play();
                }
            }

            if (CameraFollowOn)
            {
                CameraManager.pInstance.pTargetPosition = Position;
            }

            base.Update();
        }

        protected virtual void DoJumpRoutine(bool WantsJump)
        {
            // Check for JUMP
            if (WantsJump)
            {
                if ((IsGrounded && !IsJumpPressed) || (!IsGrounded && TickSinceGrounded < UngroundedJumpTickCount) || (IsJumping && !IsJumpResetRequired && CurrentJumpHoldTime < MaxJumpHoldTime))
                {
                    CurrentJumpHoldTime++;
                    CurVel.Y = -JumpLaunchVel;
                    IsJumping = true;
                }

                IsJumpPressed = true;
            }
            else
            {
                if (IsGrounded)
                {
                    CurrentJumpHoldTime = 0;
                }
                if (IsJumping)
                {
                    IsJumpResetRequired = true;
                }
                IsJumpPressed = false;
            }
        }

        protected virtual void DoWalkRoutine(bool WantsMoveLeft, bool WantMoveRight)
        {
            // If the user trying to move left (and not ducking which cancels out the left/right button presses).
            if (WantsMoveLeft)
            {
                // Increase the velocity to the left.
                CurVel.X -= WalkAccel;

                // Face left.
                SpriteFX = SpriteEffects.FlipHorizontally;

                if (!IsSprinting)
                {
                    SetNextAnim(Anim_Walk);
                }
                else
                {
                    SetNextAnim(Anim_Sprint);
                }

                // A direction key is being pressed.
                IsDirPressed = true;
            }
            else if (WantMoveRight)
            {
                CurVel.X += WalkAccel;
                SpriteFX = SpriteEffects.None;
                if (!IsSprinting)
                {
                    SetNextAnim(Anim_Walk);
                }
                else
                {
                    SetNextAnim(Anim_Sprint);
                }
                IsDirPressed = true;
            }
            else
            {
                // No direction keys are being pressed (or duck is held) so start slowing down.
                //

                IsDirPressed = false;

                // Switch to the standing animation (will be overwritten with duck later).
                SetNextAnim(Anim_Walk);

                // The is no air friction.
                if (IsGrounded)
                {
                    if (Math.Abs(CurVel.X) < WalkAccel)
                    {
                        // If we attempt to decelerate again we will go passed 0, so just jump there now.
                        CurVel.X = 0;

                        SetNextAnim(Anim_Idle);
                    }
                    else
                    {
                        // Slow down at the same rate we speed up.
                        CurVel.X -= Math.Sign(CurVel.X) * WalkAccel;
                    }
                }
            }
        }

        protected virtual void MoveWithPhysics()
        {
            Position.X += CurVel.X;
            Position.Y += CurVel.Y;
        }

        protected virtual void CollideUp()
        {
            if (CurVel.Y <= 0)
            {
                // Offset from the origin where we should check for collisions.
                Point[] headOffsets = new Point[]
                {
                    //new Point(1, -7),
                    //new Point(-1, -7),
                    new Point(0, -8),
                };

                foreach (Point P in headOffsets)
                {
                    Point finalPos = new Point((int)Math.Round(Position.X + P.X), (int)Math.Round(Position.Y + P.Y));
                    if (Game.GetTileTypeAtPos(finalPos.X, finalPos.Y, Game1.LyrCol) == Game1.SOL)
                    {
                        Position.Y = ((finalPos.Y / Game1.TileSize) * Game1.TileSize + Game1.TileSize) - P.Y;
                        CurVel.Y = Math.Max(0, CurVel.Y);
                        IsJumpResetRequired = true;
                        break;
                    }
                }
            }
        }

        protected virtual bool CollideDown()
        {
            // Left/Right collision is not performed when on ramps, to avoid hitting collision blocks
            // under the ramp.
            // TODO: This may not be needed if moved to 16x16 tiles.
            bool OnRamp = false;
            {
                bool HitSolid = false;

                // Called to check if we are moving air to ground.
                Action HandleLanding = () =>
                {
                    // We have jump landed, so instantly slow down, to make it feel less
                    // like you are landing on ice.
                    if (!IsGrounded && !IsDirPressed)
                    {
                        CurVel.X *= 0.5f;
                    }
                };

                // When touching the ground, set all the needed properties.
                Action<float> OnTouchingGround = (float YPos) =>
                {
                    Position.Y = YPos;

                    IsGrounded = true;
                    TickSinceGrounded = 0;
                    IsJumping = false;
                    IsJumpResetRequired = false;
                    if (!IsDashing)
                    {
                        IsDashResetRequired = false;
                    }
                    HitSolid = true;
                    CurVel.Y = 0;

                    if (FallingHard)
                    {
                        PostProcessScalarTween PP = new PostProcessScalarTween(Game, 1, 0,
                            15, "LavaColorScale", Game.PostProcessEffect);
                        Game.SpritesToAdd.Add(PP);

                        FellHard = true;
                        TicksSinceFellHard = 0;
                        FallingHard = false;
                    }
                };

                // When touching a ramp, set all the needed properties.
                Action<float> OnTouchingRamp = (float YPos) =>
                {
                    OnTouchingGround(YPos);
                    OnRamp = true;
                };

                // If the object is standing directly on a Ramp, offset the position of the object
                // up to the point on the ramp that they are standing.
                Func<Point, int, TmxLayerTile, bool> HandleRamp = (Point finalPos, int TileID, TmxLayerTile Tile) =>
                {
                    if (Game.IsRampRight(TileID))
                    {
                        // Find the different between the starting poisition of the tile, and the position
                        // of the object. This delta tells us how far into the tile the object is standing
                        // and thus allows us to look up the slope value at that offset.
                        float tileX = (finalPos.X / Game1.TileSize * Game1.TileSize);
                        float delta = finalPos.X - tileX;
                        int offsetIndex = (int)delta;

                        // If the tile is flipped (a slop going up to the left) we need to flip to slope offset
                        // index, since it will be read right to left, instead of left to right.
                        if (Tile.HorizontalFlip)
                        {
                            // Offsets are stored in an array from 0 -> n-1.
                            offsetIndex = Game1.TileSize - 1 - offsetIndex;
                        }

                        // Make sure this tile has registered its slope data.
                        if (Game.SlopOffsets.ContainsKey(TileID))
                        {
                            // delta is now the Y offset from the bottom of the tile.
                            delta = Game.SlopOffsets[TileID].Offsets[offsetIndex];
                        }

                        // Find the bottom position of the tile, and then offset that by the slope data.
                        float tileTop = (finalPos.Y / Game1.TileSize * Game1.TileSize);
                        float tileBottom = tileTop + Game1.TileSize - 1; // - 1 because adding a full tile size would put it into the next tile.
                        float tileSlopeY = tileBottom - delta;

                        // Only clamp the player to the slop position if they are below it. This is to avoid 
                        // a "snapping" effect, the moment they enter the 8x8 tile. However, if they are already
                        // grounded, we DO snap, so that the object hugs the curves of the slopes (not relying on
                        // gravity to bring them down).
                        if (IsGrounded || finalPos.Y >= tileSlopeY)
                        {
                            HandleLanding();
                            OnTouchingRamp(tileSlopeY);
                            return true;
                        }
                    }
                    return false;
                };

                // Only collide with ground when moving downwards.
                if (CurVel.Y >= 0)
                {
                    Point[] feetOffsets = new Point[]
                    {
                        //new Point(1, 0),
                        //new Point(-4, 0),
                        new Point(0, 0),
                    };

                    foreach (Point P in feetOffsets)
                    {
                        TmxLayerTile Tile = null;

                        // Start by finding the tile we are actually standing on. The most basic, obvious collision check.
                        Point finalPos = new Point((int)Math.Round(Position.X + P.X), (int)Math.Round(Position.Y + P.Y));

                        // Get the type of tile at the object's position.
                        int TileID = Game.GetTileTypeAtPos(finalPos.X, finalPos.Y, Game1.LyrCol, ref Tile);

                        if (TileID == Game1.SOL)
                        {
                            HandleLanding();

                            // This is a flat tile, so just put that player on top of it.
                            OnTouchingGround(finalPos.Y / Game1.TileSize * Game1.TileSize);
                        }
                        else if (HandleRamp(finalPos, TileID, Tile))
                        {
                            break;
                        }

                        // If we are currently grounded we want the object to hug the curves of the ramps. To do this (after moving left/right)
                        // the object checks the tiles above and below its current position. If it finds a ramp there, it clamps to that ramp.
                        if (IsGrounded)
                        {
                            // We may be colliding with the solid block just below a ramp. In that case, we want to shift the
                            // object to to the ramp. 

                            // Look at the tile directly above the one we are standing on.
                            finalPos = new Point((int)Math.Round(Position.X + P.X), (int)Math.Round(Position.Y - Game1.TileSize + P.Y));

                            // Get the type of that tile.
                            TileID = Game.GetTileTypeAtPos(finalPos.X, finalPos.Y, Game1.LyrCol, ref Tile);

                            // Handle the case that it is a ramp.
                            if (HandleRamp(finalPos, TileID, Tile))
                            {
                                break;
                            }

                            // Now do the same thing for the tile below us.
                            finalPos = new Point((int)Math.Round(Position.X + P.X), (int)Math.Round(Position.Y + Game1.TileSize + P.Y));
                            TileID = Game.GetTileTypeAtPos(finalPos.X, finalPos.Y, Game1.LyrCol, ref Tile);
                            if (HandleRamp(finalPos, TileID, Tile))
                            {
                                break;
                            }
                        }
                    }
                }

                // We are not currently colliding with any floors, so apply gravity and flag the object as not being on the ground.
                if (!HitSolid && !IsDashing)
                {
                    CurVel.Y += Gravity;
                    IsGrounded = false;
                }
            }

            // We do not do some additional collision in the case of being on a ramp.
            return OnRamp;
        }

        protected virtual bool CollideLeft(bool IsMovingLeft, Vector2 StartingPos)
        {
            if (IsMovingLeft)
            {
                Point[] sideOffsets = new Point[]
                {
                    //new Point(-2, -6),
                    //new Point(-2, -1),
                    new Point(-2, -4),
                };

                return CollideSide(sideOffsets, StartingPos);
            }

            return false;
        }

        protected virtual bool CollideRight(bool IsMovingRight, Vector2 StartingPos)
        {
            if (IsMovingRight)
            {
                Point[] sideOffsets = new Point[]
                {
                    //new Point(2, -6),
                    //new Point(2, -1),
                    new Point(2, -4),
                };

                return CollideSide(sideOffsets, StartingPos);
            }

            return false;
        }

        private bool CollideSide(Point[] SideOffsets, Vector2 StartingPos)
        {
            foreach (Point P in SideOffsets)
            {
                Point finalPos = new Point((int)Math.Round(Position.X + P.X), (int)Math.Round(Position.Y + P.Y));
                if (Game.GetTileTypeAtPos(finalPos.X, finalPos.Y, Game1.LyrCol) == Game1.SOL)
                {
                    Position.X = StartingPos.X;
                    CurVel.X = 0;
                    return true;
                }
            }

            return false;
        }

        public bool OnHitBouncer(Bouncer Other)
        {
            bool DidBounce = false;
            if (IsDashing)
            {
                Position.X -= CurVel.X;
                DoBounce();
                DidBounce = true;
            }
            else if (Other.KillsOnContact)
            {
                OnEnterLava();
                DidBounce = true;
            }

            return DidBounce;
        }

        protected void DoBounce()
        {
            CurVel.X = 0;
            CurVel.Y = -JumpLaunchVel * DashBounceJumpMod;
            TicksSinceDashStarted = DashTickLength;

            IsDashResetRequired = false;

            QuickEffect TeleportFlash = new QuickEffect(Game)
            {
                SpriteSheet = Game.Content.Load<Texture2D>("explosion2"),
                SpriteCell = new Point(16, 16),
                Origin = new Vector2(16 / 2, 12),
            };
            TeleportFlash.Animations.Add("Idle", new Sprite.Anim(3, 2, 3, true));
            TeleportFlash.SetNextAnim("Idle");
            TeleportFlash.Position = Position;

            BounceSound.Play(SoundVolume, 0, 0);

            Game.SpritesToAdd.Add(TeleportFlash);
        }
    }
}
