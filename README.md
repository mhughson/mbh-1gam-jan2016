# Dash Maximus #

Dash Maximus is a simple retro platforming game, written in C# using [MonoGame 3.4](http://www.monogame.net/2015/04/29/monogame-3-4/). You will need to have MonoGame 3.4 installed in order to build this project.

### What is JanGame? ###

You will find that the project is often refered to as "JanGame" inside the code base. This is because the game was created throughout the month of January, 2016, as the first game in a the [One Game a Month](http://www.onegameamonth.com/) challenge.

### Credits ###

Created by:

* Matt Hughson - http://www.matthughson.com/

Public Domain Content:

* Art: Adam Atomic - http://adamatomic.com/abandonauts/
* Music: ORTJunkie - https://archive.org/details/EerieCreepyAndScaryMusicForYourScoresDvds

### Tools Used ###

* [MonoGame](http://www.monogame.net/)
* [bfxr](http://www.bfxr.net/)
* [Twisted Wave](https://twistedwave.com/online/)
* [Tiled Level Editor](http://www.mapeditor.org/)
* [TiledSharp](https://github.com/marshallward/TiledSharp) (Tiled Runtime)