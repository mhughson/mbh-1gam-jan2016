﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JanGame
{
    struct DebugToggle
    {
        bool Enabled;

        bool KeyPressed;

        Keys ToggleKey;

        public DebugToggle(Keys Key, bool IsEnabled = false)
        {
            ToggleKey = Key;
            Enabled = IsEnabled;
            KeyPressed = false;
        }

        public bool IsEnabled()
        {
            return Enabled;
        }

        public void Update()
        {
#if DEBUG
            if (Keyboard.GetState().IsKeyDown(ToggleKey))
            {
                if (!KeyPressed)
                {
                    Enabled = !Enabled;
                }
                KeyPressed = true;
            }
            else
            {
                KeyPressed = false;
            }
#endif
        }
    }
}