﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace JanGame
{
    class PostProcessScalarTween : Sprite
    {
        /// <summary>
        /// The name of the FX property to tween.
        /// </summary>
        string PropertyName;

        /// <summary>
        /// How much the CurValue should change every tick.
        /// </summary>
        float TickDelta;

        /// <summary>
        /// The current value of the property.
        /// </summary>
        float CurValue;

        /// <summary>
        /// The target value of the property.
        /// </summary>
        float EndValue;

        /// <summary>
        /// The effect object to tween a property on.
        /// </summary>
        Effect FX;
        
        /// <summary>
        /// A function to call once this completes. Allows for daisy-chaining tweens.
        /// </summary>
        Action OnComplete;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="StartValue"></param>
        /// <param name="EndValueIn"></param>
        /// <param name="Ticks"></param>
        /// <param name="Name"></param>
        public PostProcessScalarTween(Game1 GameIn, float StartValue, float EndValueIn, float Ticks, string Name, Effect FXIn, Action OnCompleteIn = null) : base(GameIn)
        {
            CurValue = StartValue;
            EndValue = EndValueIn;
            TickDelta = (EndValue - StartValue) / Ticks;
            PropertyName = Name;
            FX = FXIn;
            OnComplete = OnCompleteIn;
        }

        /// <summary>
        /// See base.
        /// </summary>
        public override void Update()
        {
            // Update the current value locally (actually property gets updated in Draw).
            CurValue += TickDelta;

            // Check if we have reache the target value, based on the "direction" of the delta.
            if ((TickDelta > 0 && CurValue > EndValue) || (TickDelta < 0 && CurValue < EndValue))
            {
                if (OnComplete != null)
                {
                    OnComplete.Invoke();
                }
                Game.SpritesToRemove.Add(this);
            }

            base.Update();
        }

        /// <summary>
        /// See base.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            // Update the PP property with the current value.
            if (FX.Parameters[PropertyName] != null)
            {
                FX.Parameters[PropertyName].SetValue((float)CurValue);
            }

            base.Draw(spriteBatch, gameTime);
        }
    }
}
