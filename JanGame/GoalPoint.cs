﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class GoalPoint : Sprite
    {
        /// <summary>
        /// The only other object the Goal really cares about.
        /// </summary>
        Mario Player;

        /// <summary>
        /// The map which will be loaded if the player reaches this goal.
        /// </summary>
        string NextMap;

        /// <summary>
        /// Custom TMX properties.
        /// </summary>
        const string PROP_NextMap = "NextMap";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="PlayerIn"></param>
        /// <param name="ObjIn"></param>
        public GoalPoint(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) 
            : base(GameIn)
        {
            Player = PlayerIn;

            // Create the bounds bases on what was specified in the map.
            Bounds = new Rectangle((int)ObjIn.Width, (int)ObjIn.Height);

            // Note: 
            //Bounds.pTopLeft = new Vector2((float)ObjIn.X, (float)ObjIn.Y);
            //Position = new Vector2((float)ObjIn.X + Origin.X, (float)ObjIn.Y + Origin.Y);
            //Bounds.pTopLeft = Position - Origin;

            if (ObjIn.Properties.ContainsKey(PROP_NextMap))
            {
                NextMap = ObjIn.Properties[PROP_NextMap];
            }

            SpriteEffect = Game.Content.Load<Effect>("SpriteEffect_Pulse").Clone();

            Priority = PRIORITY_Background;
        }

        /// <summary>
        /// See parent.
        /// </summary>
        public override void Update()
        {
            if (!string.IsNullOrEmpty(NextMap) && Bounds.Intersects(Player.Bounds))
            {
                OnHitGoal();
            }

            base.Update();
        }

        public void OnHitGoal()
        {
            Player.OnHitGoal(this);
        }

        public void OnTransitionToGoalResult()
        {
            Game.SetMapToLoad(NextMap);
        }
    }
}
