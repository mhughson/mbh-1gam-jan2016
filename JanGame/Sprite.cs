﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JanGame
{
    public class Sprite
    {
        public struct Anim
        {
            public Anim(int CellNumberIn)
            {
                StartingCellNumber = CellNumberIn;
                NumCells = 1;
                CurrentTickCount = 0;
                TicksPerFrame = 0;
                CurrentCellNumber = 0;
                Loops = true;
            }

            public Anim(int StartingCellNumberIn, int NumCellsIn, int TicksPerFrameIn, bool LoopsIn)
            {
                StartingCellNumber = StartingCellNumberIn;
                NumCells = NumCellsIn;
                CurrentTickCount = 0;
                TicksPerFrame = TicksPerFrameIn;
                CurrentCellNumber = 0;
                Loops = LoopsIn;
            }

            public static bool operator ==(Anim Left, Anim Right)
            {
                return Left.StartingCellNumber == Right.StartingCellNumber && Left.NumCells == Right.NumCells && Left.TicksPerFrame == Right.TicksPerFrame && Left.Loops == Right.Loops;
            }

            public static bool operator !=(Anim Left, Anim Right)
            {
                return !(Left == Right);
            }

            public override bool Equals(object Other)
            {
                try
                {
                    return (bool)(this == (Anim)Other);
                }
                catch
                {
                    return false;
                }
            }

            public void Reset()
            {
                CurrentTickCount = 0;
                CurrentCellNumber = 0;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns>True if looped this update.</returns>
            public bool Update()
            {
                bool Looped = false;

                CurrentTickCount++;

                if (CurrentTickCount > TicksPerFrame)
                {
                    CurrentTickCount = 0;

                    CurrentCellNumber++;

                    if (CurrentCellNumber >= NumCells)
                    {
                        if (Loops)
                        {
                            CurrentCellNumber = 0;
                        }
                        else
                        {
                            CurrentCellNumber = NumCells - 1;
                        }
                        Looped = true;
                    }
                }

                return Looped;
            }

            public int pCurrentCell
            {
                get
                {
                    return CurrentCellNumber + StartingCellNumber;
                }
            }

            int StartingCellNumber;

            int NumCells;

            int CurrentTickCount;

            int TicksPerFrame;

            int CurrentCellNumber;

            bool Loops;
        }

        /// <summary>
        /// A list of all the animations this sprite has.
        /// </summary>
        public Dictionary<string, Anim> Animations = new Dictionary<string, Anim>();

        /// <summary>
        /// Currently visible animation key into Animations property.
        /// </summary>
        protected Anim CurrentAnim;

        /// <summary>
        /// Which animation was requested to be switched to next update.
        /// </summary>
        protected string NextAnim;

        /// <summary>
        /// The position of the object in world space.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The amound of rotation that should be applied to this sprite when rendering.
        /// </summary>
        public float RotationRadians = 0;

        /// <summary>
        /// A volume around the object used for simple collision checks.
        /// </summary>
        public Rectangle Bounds = new Rectangle();

        /// <summary>
        /// The scale of the sprite.
        /// </summary>
        public float Scale = 1;

        /// <summary>
        /// The sprite to draw.
        /// </summary>
        public Texture2D SpriteSheet;

        /// <summary>
        /// The size of a single cell in the sprint sheet. Currently every cell in the sprite sheet must be the same size.
        /// </summary>
        public Point SpriteCell;

        /// <summary>
        /// The offset into the sprite to use as the orgin of the object. Usually the feet.
        /// </summary>
        public Vector2 Origin;

        /// <summary>
        /// Sprite special effects to use.
        /// </summary>
        public SpriteEffects SpriteFX;

        /// <summary>
        /// Reference to the main Game object.
        /// </summary>
        public Game1 Game;

        protected Vector2 DebugPos;
        protected Texture2D DebugPosTexture;

        /// <summary>
        /// The effect applied to sprites.
        /// </summary>
        public Effect SpriteEffect;

        /// <summary>
        /// Mostly unused.
        /// </summary>
        public string Name;

        /// <summary>
        /// Priorities.
        /// </summary>
        public const float PRIORITY_Normal = 0.5f;
        public const float PRIORITY_Background = 0.25f;

        /// <summary>
        /// Controls the order that sprites are updated and rendered.
        /// O->1 (1 highest priority, 0 lowest priority).
        /// </summary>
        public float Priority = PRIORITY_Normal;

        public Sprite(Game1 GameIn)
        {
            Game = GameIn;

            DebugPosTexture = new Texture2D(GameIn.GraphicsDevice, 1, 1);
            Color[] data = new Color[1];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = Color.Magenta;
            }
            DebugPosTexture.SetData(data);

            // Effects must be cloned to avoid everyone changing the properties on each other.
            SpriteEffect = Game.Content.Load<Effect>("SpriteEffect").Clone();
        }

        public virtual void Update()
        {
            ChangeToNextAnim();

            CurrentAnim.Update();

            Bounds.pTopLeft = Position - Origin;
        }

        /// <summary>
        /// Draws the sprite/
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (SpriteSheet != null)
            {
                if (SpriteEffect.Parameters["MSTimeElapsed"] != null)
                {
                    SpriteEffect.Parameters["MSTimeElapsed"].SetValue((float)gameTime.TotalGameTime.TotalSeconds);
                }
                SpriteEffect.CurrentTechnique.Passes[0].Apply();

                spriteBatch.Draw(SpriteSheet, Position, new Microsoft.Xna.Framework.Rectangle(SpriteCell.X * CurrentAnim.pCurrentCell, 0, SpriteCell.X, SpriteCell.Y), Color.White, RotationRadians, Origin, Scale, SpriteFX, 0);
            }

            //spriteBatch.Draw(DebugPosTexture, Position, Color.White);

            //spriteBatch.Draw(SpriteSheet, DebugPos, new Rectangle(SpriteCell.X * CurrentAnim.pCurrentCell, 0, SpriteCell.X, SpriteCell.Y), Color.White, 0, Origin, 1, SpriteFX, 0);

            //spriteBatch.Draw(DebugPosTexture, DebugPos, Color.White);
        }

        protected virtual void ChangeToNextAnim()
        {
            if (!String.IsNullOrEmpty(NextAnim) && Animations.ContainsKey(NextAnim) && CurrentAnim != Animations[NextAnim])
            {
                CurrentAnim = Animations[NextAnim];
                //CurrentAnim.Reset();
            }
            NextAnim = "";
        }

        public virtual void SetNextAnim(string NewAnim)
        {
            if (Animations.ContainsKey(NewAnim))
            {
                NextAnim = NewAnim;
                //CurrentAnim.Reset();
            }
        }

        public virtual void FinalizeLoad()
        {

        }
    }
}
