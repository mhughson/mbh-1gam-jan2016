﻿using System;
using System.Windows.Forms;

namespace JanGame
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Only run on Win7 or better.
            if (System.Environment.OSVersion.Version.Major < 6 || (System.Environment.OSVersion.Version.Major == 6 && System.Environment.OSVersion.Version.Minor < 1))
            {
                System.Windows.Forms.MessageBox.Show("This game requires windows 7 or greater.");
                return;
            }


            Launcher L = new Launcher();
            DialogResult dialogResult = L.ShowDialog();

            //DialogResult dialogResult = MessageBox.Show("Would you like to run the #lowresjam version of Dash Maxiumus?", "Select Version", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                string[] newArgs = new string[args.Length + 1];
                newArgs[args.Length] = "lowrezjam";
                args = newArgs;
            }
            

            using (var game = new Game1(args))
                game.Run();
        }
    }
#endif
}
