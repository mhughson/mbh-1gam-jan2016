sampler TextureSampler : register(s0);
float MSTimeElapsed = 0;
//float3 LavaColor = float3(0.890, 0.141, 0.003);
float3 LavaColor = float3(1,1,1);
float LavaColorScale = 0;
float ScanLineMod = 0.05;

float4 main(float4 position : SV_Position, float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
	// Look up the texture color.
	float4 tex = tex2D(TextureSampler, texCoord);

	tex.rgb = lerp(tex.rgb, LavaColor, LavaColorScale);

	// Convert it to greyscale. The constants 0.3, 0.59, and 0.11 are because
	// the human eye is more sensitive to green light, and less to blue.
	float3 greyscale = dot(tex.rgb, float3(0.3, 0.59, 0.11));

	// Determine the min value for our desaturation pulse.
	float3 desatMin = lerp(greyscale, tex.rgb, 0.8);

	// Pulse the saturation levels.
	tex.rgb = lerp(desatMin, tex.rgb, smoothstep(-1, 1, sin(MSTimeElapsed * 3)));

	// Add scanlines every other line in the Y.
	// Make 1 variable to adjust how many pixels in each group of scanlines.
	tex.rgb -= (floor(((position.y / 1) % 2))) * ScanLineMod;

	return tex;
}


technique Technique1
{
	pass Pass1
	{
		PixelShader = compile ps_4_0 main();
	}
}