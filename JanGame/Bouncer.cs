﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class Bouncer : Sprite
    {
        /// <summary>
        /// A reference to the player for collision checks.
        /// </summary>
        Mario Player;

        /// <summary>
        /// Is the Bouncer currently allowing for bounces.
        /// </summary>
        public bool IsEnabled = true;

        /// <summary>
        /// Tracks how long the Bouncer has been disabled.
        /// </summary>
        int TicksSinceDisabled = 0;

        /// <summary>
        /// How long the Bouncer should wait before re-enabling itself.
        /// </summary>
        const int TicksToReenable = 500;

        /// <summary>
        /// Is this an object that should kill the player if touched while not dashing.
        /// </summary>
        public bool KillsOnContact = false;

        /// <summary>
        /// Animations.
        /// </summary>
        public const string Anim_Enabled = "Enabled";
        public const string Anim_Disabled = "Disabled";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="PlayerIn"></param>
        public Bouncer(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn)
        {
            Player = PlayerIn;
            Priority = PRIORITY_Background;
            Name = ObjIn.Name;
        }

        /// <summary>
        /// See parent.
        /// </summary>
        public override void Update()
        {
            base.Update();

            // Only attempt bounces if the Bouncer is enabled.
            if (IsEnabled)
            {
                // Are we touching the player?
                if (Bounds.Intersects(Player.Bounds))
                {
                    // Attempt a bounce. Will check if player is in the right state (eg. dashing, etc), and return
                    // true of the player was bounced up.
                    if (Player.OnHitBouncer(this))
                    {
                        // Disable the object, and start the timer to re-enable it later.
                        IsEnabled = false;
                        TicksSinceDisabled = 0;
                        SetNextAnim(Anim_Disabled);
                    }
                }
            }
            else
            {
                // Count up to the time when this object should be re-enabled again.
                TicksSinceDisabled++;

                if (TicksSinceDisabled >= TicksToReenable)
                {
                    IsEnabled = true;
                    SetNextAnim(Anim_Enabled);
                }
            }
        }
    }
}
