﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class Laser : Sprite
    {
        /// <summary>
        /// The player we will try to kill!
        /// </summary>
        Mario Player;

        /// <summary>
        /// The TmxObj used to define this object.
        /// </summary>
        TmxObject Obj;

        /// <summary>
        /// List of all the sprites used to draw the beam portion of the laser.
        /// </summary>
        protected List<Sprite> Beam;

        /// <summary>
        /// The starting socket image.
        /// </summary>
        protected Sprite BeamSocketStart;

        /// <summary>
        /// The ending socket image.
        /// </summary>
        protected Sprite BeamSocketEnd;

        /// <summary>
        /// Tracks of this beam is shooting horizontally (or vertically). Determined based on size of beam.
        /// </summary>
        bool IsHorizontal;

        bool IsTurningOn = false;
        int TurnOnIndex = 0;
        int NumBeams = 0;

        /// <summary>
        /// Tracks whether or not the beam is turned on.
        /// </summary>
        protected bool IsEnabled = true;

        /// <summary>
        /// Which way the beam should flip when animating. Changes based on IsHorizontal.
        /// </summary>
        SpriteEffects BeamFXRequired;

        List<Sprite> Switches;

        SoundEffect PowerOn;
        SoundEffect PowerOff;

        /// <summary>
        /// Beam Animations.
        /// </summary>
        const string Anim_Vert = "Vert";
        const string Anim_Horz = "Horz";

        /// <summary>
        /// Socket Animations.
        /// </summary>
        const string Anim_BottomOff = "BottomOff";
        const string Anim_BottomOn = "BottomOn";
        const string Anim_TopOff = "TopOff";
        const string Anim_TopOn = "TopOn";
        const string Anim_RightOff = "RightOff";
        const string Anim_RightOn = "RightOn";
        const string Anim_LeftOff = "LeftOff";
        const string Anim_LeftOn = "LeftOn";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="PlayerIn"></param>
        /// <param name="ObjIn"></param>
        public Laser(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn)
        {
            Obj = ObjIn;
            Beam = new List<Sprite>();
            Switches = new List<Sprite>();
            Player = PlayerIn;
            Position = new Vector2((float)ObjIn.X, (float)ObjIn.Y);
            IsHorizontal = (Obj.Width > Obj.Height);

            PowerOn = Game.Content.Load<SoundEffect>("snd_lava_on");
            PowerOff = Game.Content.Load<SoundEffect>("snd_lava_off");

            Func<Sprite> CreateBeamSocket = () =>
            {
                Sprite S = new Sprite(Game)
                {
                    SpriteSheet = Game.Content.Load<Texture2D>("beamsocket"),
                    SpriteCell = new Point(8, 8),
                    Origin = new Vector2(0, 0),
                    Position = Position,
                };
                S.Animations.Add(Anim_BottomOff, new Anim(0));
                S.Animations.Add(Anim_BottomOn, new Anim(1));
                S.Animations.Add(Anim_TopOff, new Anim(2));
                S.Animations.Add(Anim_TopOn, new Anim(3));
                S.Animations.Add(Anim_RightOff, new Anim(4));
                S.Animations.Add(Anim_RightOn, new Anim(5));
                S.Animations.Add(Anim_LeftOff, new Anim(6));
                S.Animations.Add(Anim_LeftOn, new Anim(7));
                return S;
            };

            // Create the sockets that appear at the start and end of the beam. These appear regardless of whether or
            // not the beam is turned on.
            BeamSocketStart = CreateBeamSocket();
            BeamSocketStart.Position = Position;
            Game.SpritesToAdd.Add(BeamSocketStart);

            BeamSocketEnd = CreateBeamSocket();
            if (IsHorizontal)
            {
                BeamSocketEnd.Position = Position + new Vector2((float)Obj.Width - 8, 0);
            }
            else
            {
                BeamSocketEnd.Position = Position + new Vector2(0, (float)Obj.Height - 8);
            }
            Game.SpritesToAdd.Add(BeamSocketEnd);

            TurnOnBeam();
        }

        protected void TurnOnBeam()
        {
            float BoundsSize = 2.0f;

            int BeamSize = 4;

            // Is Horizontal.
            if (Obj.Width > Obj.Height)
            {
                Bounds = new Rectangle((float)Obj.Width, BoundsSize, Position - Origin);
                NumBeams = (int)Obj.Width / BeamSize;
                BeamFXRequired = SpriteEffects.FlipVertically;
            }
            else
            {
                Bounds = new Rectangle(BoundsSize, (float)Obj.Height, Position - Origin);
                NumBeams = (int)Obj.Height / BeamSize;
                BeamFXRequired = SpriteEffects.FlipHorizontally;
            }

            IsTurningOn = true;
            TurnOnIndex = 0;

            if (IsHorizontal)
            {
                BeamSocketStart.SetNextAnim(Anim_LeftOn);
                BeamSocketEnd.SetNextAnim(Anim_RightOn);
            }
            else
            {
                BeamSocketStart.SetNextAnim(Anim_TopOn);
                BeamSocketEnd.SetNextAnim(Anim_BottomOn);
            }
            if (CameraManager.pInstance.IsOnCamera(Bounds))
            {
                PowerOn.Play(0.25f, 0, 0);
            }

            SetIsEnabled(true);
        }

        protected void TurnOffBeam()
        {
            Game.SpritesToRemove.AddRange(Beam);
            Beam.Clear();

            if (IsHorizontal)
            {
                BeamSocketStart.SetNextAnim(Anim_LeftOff);
                BeamSocketEnd.SetNextAnim(Anim_RightOff);
            }
            else
            {
                BeamSocketStart.SetNextAnim(Anim_TopOff);
                BeamSocketEnd.SetNextAnim(Anim_BottomOff);
            }

            if (CameraManager.pInstance.IsOnCamera(Bounds))
            {
                PowerOff.Play(0.25f, 0, 0);
            }
            IsTurningOn = false;

            SetIsEnabled(false);
        }

        int TimeTillBeamFlip = 3;

        public override void Update()
        {
            base.Update();

            if (Switches.Count > 0)
            {
                bool FoundEnabled = false;
                foreach (Bouncer B in Switches)
                {
                    FoundEnabled |= B.IsEnabled;
                }

                SetIsEnabled(FoundEnabled);
            }

            //if (InputManager.pInstance.CheckAction(InputManager.InputActions.Y, true))
            //{
            //    SetIsEnabled(!IsEnabled);
            //}

            // The bounds wil be set to the top left of the object in base.Update() but
            // we want bounds that are slightly offset.
            if (IsHorizontal)
            {
                Bounds.pTopLeft += new Vector2(0, 3);
            }
            else
            {
                Bounds.pTopLeft += new Vector2(3, 0);
            }

            if (IsEnabled && Bounds.Intersects(Player.Bounds))
            {
                Player.OnEnterLava();
            }

            TimeTillBeamFlip--;

            if (TimeTillBeamFlip <= 0)
            {
                TimeTillBeamFlip = 5;
                foreach (Sprite S in Beam)
                {
                    if ((S.SpriteFX & BeamFXRequired) == BeamFXRequired)
                    {
                        S.SpriteFX = SpriteEffects.None;
                    }
                    else
                    {
                        S.SpriteFX = BeamFXRequired;
                    }
                }
            }

            if (IsTurningOn)
            {
                int BeamSize = 4;
                int i = TurnOnIndex;
                {
                    Sprite S = new Sprite(Game)
                    {
                        SpriteSheet = Game.Content.Load<Texture2D>("beam"),
                        SpriteCell = new Point(BeamSize, BeamSize),
                        Origin = new Vector2(0, 0),
                        Position = Position,
                        Priority = PRIORITY_Background,
                    };
                    S.Animations.Add(Anim_Vert, new Anim(0));
                    S.Animations.Add(Anim_Horz, new Anim(1));

                    if (IsHorizontal)
                    {
                        S.Position += new Vector2(i * BeamSize, BeamSize * 0.5f);
                        S.SetNextAnim(Anim_Horz);
                    }
                    else
                    {
                        S.Position += new Vector2(BeamSize * 0.5f, i * BeamSize);
                        S.SetNextAnim(Anim_Vert);
                    }


                    S.SpriteEffect = Game.Content.Load<Effect>("SpriteEffect_Pulse").Clone();
                    S.SpriteEffect.Parameters["Tint"].SetValue(new Vector4(247.0f / 255.0f, 225.0f / 255.0f, 118.0f / 255.0f, 1.0f));
                    S.SpriteEffect.Parameters["Intensity"].SetValue(0.50f);
                    Game.SpritesToAdd.Add(S);
                    Beam.Add(S);
                }
                TurnOnIndex++;

                if (TurnOnIndex >= NumBeams)
                {
                    IsTurningOn = false;
                }
            }
        }

        public void SetIsEnabled(bool Enabled)
        {
            if (Enabled != IsEnabled)
            {
                IsEnabled = Enabled;

                if (IsEnabled)
                {
                    TurnOnBeam();
                }
                else
                {
                    TurnOffBeam();
                }
            }
        }

        public override void FinalizeLoad()
        {
            base.FinalizeLoad();

            if (Obj.Properties.ContainsKey("Switches"))
            {
                List<string> SwitchNames = SwitchNames = Obj.Properties["Switches"].Split(new Char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                if (SwitchNames.Count > 0)
                {
                    // Assuming the switches are in the add list for now.
                    foreach (Sprite S in Game.SpritesToAdd)
                    {
                        if (S.GetType().IsSubclassOf(typeof(Bouncer)) || S.GetType() == typeof(Bouncer))
                        {
                            if (SwitchNames.Contains(S.Name))
                            {
                                Switches.Add(S);
                            }
                        }
                    }
                }
            }
        }
    }
}
