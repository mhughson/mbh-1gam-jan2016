﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class Idol : Trigger
    {

        public Vector2 FloatStart;
        public Vector2 FloatDest;
        public float FloatTime = 120;
        public float CurrentFloatTime = 0;
        SoundEffect EarthquakeSound;

        LaserSweeper Laser;
        Laser Door;

        public Idol(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn, PlayerIn, ObjIn)
        {
            SpriteEffect = Game.Content.Load<Effect>("SpriteEffect_Pulse").Clone();

            SpriteEffect.Parameters["Tint"].SetValue(new Vector4(1, 1, 1, 1));
            SpriteEffect.Parameters["Intensity"].SetValue(0.50f);

            EarthquakeSound = Game.Content.Load<SoundEffect>("snd_earthquake");

        }

        public override void Update()
        {
            base.Update();

            CurrentFloatTime++;

            Position = Vector2.SmoothStep(FloatStart, FloatDest, CurrentFloatTime / FloatTime);

            if ((Position - FloatDest).Length() < 0.1f)
            {
                FlipFloatDest();
            }
        }

        public override void FinalizeLoad()
        {
            base.FinalizeLoad();

            FloatDest = FloatStart = Position;
            FloatDest.Y -= 8;
            
            if (Obj.Properties.ContainsKey("Laser"))
            {
                string LaserName = Obj.Properties["Laser"];

                if (!string.IsNullOrEmpty(LaserName))
                {
                    // Assuming the switches are in the add list for now.
                    foreach (Sprite S in Game.SpritesToAdd)
                    {
                        if (S.GetType().IsSubclassOf(typeof(LaserSweeper)) || S.GetType() == typeof(LaserSweeper))
                        {
                            Laser = S as LaserSweeper;
                        }
                    }
                }
            }

            if (Obj.Properties.ContainsKey("Door"))
            {
                string DoorName = Obj.Properties["Door"];

                if (!string.IsNullOrEmpty(DoorName))
                {
                    // Assuming the switches are in the add list for now.
                    foreach (Sprite S in Game.SpritesToAdd)
                    {
                        if (S.GetType().IsSubclassOf(typeof(Laser)) || S.GetType() == typeof(Laser))
                        {
                            Door = S as Laser;
                        }
                    }
                }
            }
        }

        private void FlipFloatDest()
        {
            Vector2 Temp = FloatStart;
            FloatStart = FloatDest;
            FloatDest = Temp;

            CurrentFloatTime = 0;
        }

        public override void OnTriggered()
        {
            Game.SpritesToRemove.Add(this);

            Laser.SetIsEnabled(true);
            Door.SetIsEnabled(false);

            CameraManager.pInstance.StartCameraShake(120, 1.0f);
            EarthquakeSound.Play(0.25f, 0, 0);
        }
    }
}
