﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class LaserSweeper : Laser
    {
        public LaserSweeper(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn, PlayerIn, ObjIn)
        {
            TurnOffBeam();
        }

        public override void Update()
        {
            base.Update();

            if (IsEnabled)
            {
                foreach(Sprite S in Beam)
                {
                    S.Position.Y -= 0.1f;
                }

                Position.Y -= 0.1f;
                BeamSocketStart.Position.Y -= 0.1f;
                BeamSocketEnd.Position.Y -= 0.1f;
            }
        }
    }
}
