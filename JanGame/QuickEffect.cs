﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JanGame
{
    class QuickEffect : Sprite
    {
        public QuickEffect(Game1 GameIn) : base (GameIn)
        {

        }

        public override void Update()
        {
            ChangeToNextAnim();

            if (CurrentAnim.Update())
            {
                Game.SpritesToRemove.Add(this);
            }

            Bounds.pTopLeft = Position - Origin;
        }
    }
}
