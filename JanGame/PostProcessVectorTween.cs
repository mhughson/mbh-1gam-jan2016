﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JanGame
{
    class PostProcessVectorTween : Sprite
    {
        /// <summary>
        /// The name of the FX property to tween.
        /// </summary>
        string PropertyName;

        /// <summary>
        /// How much the CurValue should change every tick.
        /// </summary>
        Vector4 TickDelta;

        /// <summary>
        /// The current value of the property.
        /// </summary>
        Vector4 CurValue;

        /// <summary>
        /// The target value of the property.
        /// </summary>
        Vector4 EndValue;

        /// <summary>
        /// The effect object to tween a property on.
        /// </summary>
        Effect FX;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="StartValue"></param>
        /// <param name="EndValueIn"></param>
        /// <param name="Ticks"></param>
        /// <param name="Name"></param>
        public PostProcessVectorTween(Game1 GameIn, Vector4 StartValue, Vector4 EndValueIn, float Ticks, string Name, Effect FXIn) : base(GameIn)
        {
            CurValue = StartValue;
            EndValue = EndValueIn;
            TickDelta = (EndValue - StartValue) / Ticks;
            PropertyName = Name;
            FX = FXIn;
        }

        /// <summary>
        /// See base.
        /// </summary>
        public override void Update()
        {
            // Update the current value locally (actually property gets updated in Draw).
            CurValue += TickDelta;

            // Check if we have reache the target value, based on the "direction" of the delta.
            if ((EndValue - CurValue).Length() < 0.01f)
            {
                Game.SpritesToRemove.Add(this);
            }

            base.Update();
        }

        /// <summary>
        /// See base.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            // Update the PP property with the current value.
            if (FX.Parameters[PropertyName] != null)
            {
                FX.Parameters[PropertyName].SetValue((Vector4)CurValue);
            }

            base.Draw(spriteBatch, gameTime);
        }
    }
}
