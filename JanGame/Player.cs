﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace JanGame
{
    class Player : Sprite
    {
        // ANIMATION NAMES
        //

        public const string Anim_Idle = "idle";

        Vector2 FlyingVec;

        bool MousePressed = false;

        float Radius = 140.0f;

        protected float RadiusScaled
        {
            get
            {
                return Radius * Scale;
            }
        }

        protected float RightEdge
        {
            get
            {
                return Position.X + RadiusScaled;
            }
        }

        protected float LeftEdge
        {
            get
            {
                return Position.X - RadiusScaled;
            }
        }
        protected float BottomEdge
        {
            get
            {
                return Position.Y + RadiusScaled;
            }
        }

        protected float TopEdge
        {
            get
            {
                return Position.Y - RadiusScaled;
            }
        }

        enum States
        {
            Idle,
            Locked,
            Flying,
        }

        States CurrentState = States.Idle;

        public Player(Game1 GameIn) : base(GameIn)
        {

        }

        public override void Update()
        {
            base.Update();

            MouseState ms = Mouse.GetState();
            if (ms.LeftButton == ButtonState.Released)
            {
                MousePressed = false;
            }

            switch(CurrentState)
            {
                case States.Idle:
                    {

                        Position = ms.Position.ToVector2();

                        if (ms.LeftButton == ButtonState.Pressed && !MousePressed)
                        {
                            MousePressed = true;
                            CurrentState = States.Locked;
                        }
                        break;
                    }

                case States.Locked:
                    {
                        if (ms.LeftButton == ButtonState.Pressed && !MousePressed)
                        {
                            MousePressed = true;
                            CurrentState = States.Flying;

                            FlyingVec = (ms.Position.ToVector2() - Position) * 0.05f;
                        }

                        break;
                    }

                case States.Flying:
                    {
                        Vector2 OldPos = Position;

                        Position += FlyingVec;

                        Vector2 CollisionPos;

                        // MOVING RIGHT
                        if (FlyingVec.X > 0)
                        {
                            if (FindNextSolTile(new Vector2(RightEdge, Position.Y), FlyingVec, out CollisionPos))
                            {
                                Position = CollisionPos - new Vector2(RadiusScaled, 0);
                                FlyingVec = Reflect(FlyingVec, -Vector2.UnitX);
                            }
                        }
                        // MOVING LEFT
                        else if (FlyingVec.X < 0)
                        {
                            if (FindNextSolTile(new Vector2((LeftEdge), Position.Y), FlyingVec, out CollisionPos))
                            {
                                Position = CollisionPos + new Vector2(RadiusScaled, 0);
                                FlyingVec = Reflect(FlyingVec, Vector2.UnitX);
                            }
                        }

                        // MOVING DOWN
                        if (FlyingVec.Y > 0)
                        {
                            if (FindNextSolTile(new Vector2(Position.X, BottomEdge), FlyingVec, out CollisionPos))
                            {
                                Position = CollisionPos - new Vector2(0, RadiusScaled);
                                FlyingVec = Reflect(FlyingVec, -Vector2.UnitY);
                            }
                        }
                        // MOVING UP
                        else if (FlyingVec.Y < 0)
                        {
                            if (FindNextSolTile(new Vector2(Position.X, TopEdge), FlyingVec, out CollisionPos))
                            {
                                Position = CollisionPos + new Vector2(0, RadiusScaled);
                                FlyingVec = Reflect(FlyingVec, Vector2.UnitY);
                            }
                        }


                        if (ms.LeftButton == ButtonState.Pressed && !MousePressed)
                        {
                            MousePressed = true;
                            CurrentState = States.Idle;
                        }
                        break;
                    }
            }
        }

        public Vector2 Reflect(Vector2 vector, Vector2 normal)
        {
            return vector - 2 * Vector2.Dot(vector, normal) * normal;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.Draw(spriteBatch, gameTime);

            if (CurrentState == States.Locked)
            {
                MouseState ms = Mouse.GetState();
                spriteBatch.Draw(SpriteSheet, ms.Position.ToVector2(), new Microsoft.Xna.Framework.Rectangle(SpriteCell.X * CurrentAnim.pCurrentCell, 0, SpriteCell.X, SpriteCell.Y), Color.White * 0.25f, 0, Origin, Scale, SpriteFX, 0);
            }
        }

        protected bool FindNextSolTile(Vector2 Pos, Vector2 Direction, out Vector2 CollisionPos)
        {
            float Len = Direction.Length();
            Direction.Normalize();

            int steps = (int)Len / Game1.TileSize;

            for(int i = 0; i <= steps; i++)
            {
                if (Game.GetTileTypeAtPos((int)Pos.X, (int)Pos.Y, Game1.LyrCol) == Game1.SOL)
                {
                    CollisionPos = Pos;
                    return true;
                }
                Pos += Direction * (Game1.TileSize);
            }

            CollisionPos = Vector2.Zero;
            return false;
        }
    }
}
