﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using TiledSharp;

namespace JanGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        /// <summary>
        /// MonoGame Graphics objects.
        /// </summary>
        public GraphicsDeviceManager Graphics;

        /// <summary>
        /// Used for rendering sprites.
        /// </summary>
        SpriteBatch SpriteBatch;

        /// <summary>
        /// Named TMX layers.
        /// </summary>
        public const string LyrCol = "Collision";
        public const string LyrGam = "Gameplay";

         /// <summary>
        /// Sheet for all the sprites.
        /// </summary>
        Texture2D TileSheet;

        /// <summary>
        /// Sheet for all the collision sprites.
        /// </summary>
        Texture2D TileSheetCollision;
        Texture2D ThinFont;
        Texture2D ThickFont;

        /// <summary>
        /// The current loaded map.
        /// </summary>
        public TmxMap mCurrentTileMap;

        /// <summary>
        /// Debug flag for whether or not the collision layer should be drawn.
        /// </summary>
        DebugToggle CollisionLayerToggle;

        /// <summary>
        /// Debug flag for wther or not the tile outline layer should be pressed.
        /// </summary>
        DebugToggle TileOutlineLayerToggle;

        DebugToggle PostEffectsToggle;

        /// <summary>
        /// Random number generator.
        /// </summary>
        public Random Rand;

        /// <summary>
        /// Used to draw the Online layer.
        /// </summary>
        Texture2D DebugOutlineTile;

        /// <summary>
        /// The player.
        /// </summary>
        Mario P;

        Song SongFull;
        Song SongPunch;

        /// <summary>
        /// The width and height of a tile. Use for collision routines.
        /// </summary>
        public const int TileSize = 8;

        /// <summary>
        /// Values for special tiles.
        /// </summary>
        public const int EMP = 0;
        public const int SOL = 401;
        public const int RAMP_RIGHT_START = 402;
        public const int RAMP_RIGHT_END = 404;

        /// <summary>
        /// Stores an Y offset in pixels from the bottom of a tile. Each index
        /// maps to an pixel offset from the left side of the tile. For instance, 
        /// index 0 is the y offset of the slope on the left most point on the tile.
        /// </summary>
        public struct TileSlopeOffsets
        {
            public int[] Offsets;
        }

        /// <summary>
        /// Definitions of all known slope offsets.
        /// </summary>
        public Dictionary<int, TileSlopeOffsets> SlopOffsets;

        /// <summary>
        /// The current list of sprites that want to be added at the end of this frame.
        /// </summary>
        public List<Sprite> SpritesToAdd;

        /// <summary>
        /// The current list of sprites that want to be removed at the end of this frame.
        /// </summary>
        public List<Sprite> SpritesToRemove;

        /// <summary>
        /// List of all the sprites in the game.
        /// </summary>
        List<Sprite> Sprites;

        /// <summary>
        /// Placeholder effect.
        /// </summary>
        public Effect PostProcessEffect;

        /// <summary>
        /// Texture used for noise effect.
        /// </summary>
        Texture2D NoiseTexture;
        
        /// <summary>
        /// The render target for the game.
        /// </summary>
        RenderTarget2D PostProcessRenderTarget;

        /// <summary>
        /// Stores the name of a map that has been requested for load (if there has been a request).
        /// </summary>
        string NextMap;

        string CurrentMap;

        /// <summary>
        /// The Goal for the current map. Used for debug purposed (eg. skipping map).
        /// </summary>
        GoalPoint CurrentGoal;

        bool IsPaused = false;

        Vector2 CameraMoveDir;
        float CameraMoveSpeed = 0.2f;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Game1(string[] args)
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //Graphics.PreferredBackBufferWidth = 1024;
            //Graphics.PreferredBackBufferHeight = 960;
            //Graphics.ApplyChanges();

            //Graphics.PreferredBackBufferWidth = Graphics.GraphicsDevice.DisplayMode.Width;
            //Graphics.PreferredBackBufferHeight = Graphics.GraphicsDevice.DisplayMode.Height;
            //Graphics.IsFullScreen = true;

            Window.AllowUserResizing = true;

            CommandLineManager.pInstance.pArgs = args;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            int resScale = 4;
            int resX = 128;
            int resY = 120;

            if (CommandLineManager.pInstance["lowrezjam"] != null)
            {
                resX = 64;
                resY = 64;

                // The min window width is ~128.
                resScale = 4;
            }

            if (CommandLineManager.pInstance["resScale"] != null)
            {
                resScale = int.Parse(CommandLineManager.pInstance["resScale"]);
            }

            if (CommandLineManager.pInstance["resx"] != null)
            {
                resX = int.Parse(CommandLineManager.pInstance["resx"]);
            }
            if (CommandLineManager.pInstance["resy"] != null)
            {
                resY = int.Parse(CommandLineManager.pInstance["resy"]);
            }

            Graphics.PreferredBackBufferWidth = resX * resScale;
            Graphics.PreferredBackBufferHeight = resY * resScale;
            //Graphics.PreferredBackBufferWidth = Graphics.GraphicsDevice.DisplayMode.Width;
            //Graphics.PreferredBackBufferHeight = Graphics.GraphicsDevice.DisplayMode.Height;
            //Graphics.IsFullScreen = true;
            //Graphics.PreferMultiSampling = false;
            //Graphics.SynchronizeWithVerticalRetrace = true;
            Graphics.ApplyChanges();

            // Avoid errors.
            if (Graphics.PreferredBackBufferWidth > 0 && Graphics.PreferredBackBufferHeight > 0)
            {
                PostProcessRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight);
            }

            // Avoid the "jitter".
            // http://forums.create.msdn.com/forums/p/9934/53561.aspx#53561
            // Set to TRUE so that we can target 30fps to match windows phone.
            // Should be FALSE to fix jitter issue.
            IsFixedTimeStep = true;

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromSeconds(1 / 60.0f);

            CollisionLayerToggle = new DebugToggle(Keys.F1);
            TileOutlineLayerToggle = new DebugToggle(Keys.F2);
            PostEffectsToggle = new DebugToggle(Keys.F3, true);

            CameraManager.pInstance.Initialize(this, new Vector2(resX, resY));

            Window.ClientSizeChanged += Window_ClientSizeChanged;

            SlopOffsets = new Dictionary<int, TileSlopeOffsets>()
            {
                { RAMP_RIGHT_START,     new TileSlopeOffsets() { Offsets = new int[8] { 0,1,2,3,4,5,6,7} } },
                { RAMP_RIGHT_START+1,   new TileSlopeOffsets() { Offsets = new int[8] { 0,1,1,2,2,3,3,4} } },
                { RAMP_RIGHT_START+2,   new TileSlopeOffsets() { Offsets = new int[8] { 4,4,5,5,6,6,6,7} } },
            };

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // Preload assets.
            Content.Load<SoundEffect>("footstep");
            Content.Load<SoundEffect>("dash");
            Content.Load<SoundEffect>("bounce");
            Content.Load<SoundEffect>("death");
            Content.Load<SoundEffect>("snd_earthquake");

            Rand = new Random();

            Sprites = new List<Sprite>();
            SpritesToAdd = new List<Sprite>();
            SpritesToRemove = new List<Sprite>();

            DebugOutlineTile = new Texture2D(Graphics.GraphicsDevice, TileSize, TileSize);
            Color[] data = new Color[TileSize * TileSize];
            for (int i = 0; i < data.Length; ++i)
            {
                if (i % TileSize == 0 || (i / TileSize) % TileSize == 0 || (i - 7) % TileSize == 0 || ((i + TileSize) / TileSize) % TileSize == 0)
                {
                    data[i] = Color.White;
                }
            }
            DebugOutlineTile.SetData(data);

            TileSheet = Content.Load<Texture2D>("tiles");
            TileSheetCollision = Content.Load<Texture2D>("tilesheet_collision");
            ThinFont = Content.Load<Texture2D>("thinfont");
            ThickFont = Content.Load<Texture2D>("thickfont");

            //P = new Mario(this)
            //{
            //    SpriteSheet = Content.Load<Texture2D>("astro"),
            //    SpriteCell = new Point(8, 8),
            //    Origin = new Vector2(8 / 2, 8),
            //    Bounds = new Rectangle(8, 8),
            //};

            //P.Animations.Add(Mario.Anim_Idle, new Sprite.Anim(0));
            //P.Animations.Add(Mario.Anim_Walk, new Sprite.Anim(1, 6, 10, true));
            //P.Animations.Add(Mario.Anim_Sprint, new Sprite.Anim(1, 6, 5, true));
            //P.Animations.Add(Mario.Anim_Jump, new Sprite.Anim(7));
            //P.Animations.Add(Mario.Anim_Fall, new Sprite.Anim(8));
            //P.Animations.Add(Mario.Anim_LandHard, new Sprite.Anim(14));
            //P.Animations.Add(Mario.Anim_TryGetUp, new Sprite.Anim(9));
            //P.Scale = 1f;

            //Sprites.Add(P);

            LoadMap("TitleScreen");

            PostProcessEffect = Content.Load<Effect>("PostProcess");

            // Turn off scanlines for lowrezjam since they are not affected by scale.
            if (CommandLineManager.pInstance["lowrezjam"] != null)
            {
                PostProcessEffect.Parameters["ScanLineMod"].SetValue((float)0);
            }

            NoiseTexture = Content.Load<Texture2D>("noise");
            
            SongFull = Content.Load<Song>("classicHorror1_end");
            SongPunch = Content.Load<Song>("classicHorror1_punch");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public void Respawn()
        {
            SetMapToLoad(CurrentMap);
        }

        /// <summary>
        /// Request that a map be loaded. We can't load it right now, as we may be in the middle of updating
        /// our game objects, which would prevent us from clear out those lists.
        /// </summary>
        /// <param name="MapName"></param>
        public void SetMapToLoad(string MapName)
        {
            NextMap = MapName;
        }

        /// <summary>
        /// Clear out the current map and load a new one.
        /// </summary>
        /// <param name="MapName"></param>
        protected void LoadMap(string MapName)
        {
            CurrentMap = MapName;

            Sprites.Clear();
            SpritesToAdd.Clear();
            SpritesToRemove.Clear();

            if (CommandLineManager.pInstance["lowrezjam"] != null)
            {
                if (MapName == "EndScreen")
                {
                    MapName = "EndScreen_LowRez";
                }
            }

            mCurrentTileMap = new TmxMap("Content/" + MapName + ".tmx");

            if (mCurrentTileMap.ObjectGroups.Count > 0)
            {
                foreach (TmxObject Obj in mCurrentTileMap.ObjectGroups[0].Objects)
                {
                    switch (Obj.Type)
                    {
                        case "SpawnPoint":
                            {
                                P = new Mario(this)
                                {
                                    SpriteSheet = Content.Load<Texture2D>("astro"),
                                    SpriteCell = new Point(8, 8),
                                    Origin = new Vector2(8 / 2, 8),
                                    Bounds = new Rectangle(8, 8),
                                };

                                P.Animations.Add(Mario.Anim_Idle, new Sprite.Anim(0));
                                P.Animations.Add(Mario.Anim_Walk, new Sprite.Anim(1, 6, 10, true));
                                P.Animations.Add(Mario.Anim_Sprint, new Sprite.Anim(1, 6, 5, true));
                                P.Animations.Add(Mario.Anim_Jump, new Sprite.Anim(7));
                                P.Animations.Add(Mario.Anim_Fall, new Sprite.Anim(8));
                                P.Animations.Add(Mario.Anim_LandHard, new Sprite.Anim(14));
                                P.Animations.Add(Mario.Anim_TryGetUp, new Sprite.Anim(9));
                                P.Scale = 1f;

                                SpritesToAdd.Add(P);
                                P.Position = new Vector2((float)Obj.X, (float)Obj.Y);

                                break;
                            }

                        case "GoalPoint":
                            {
                                float GoalSize = 8;
                                CurrentGoal = new GoalPoint(this, P, Obj)
                                {
                                    SpriteSheet = Content.Load<Texture2D>("goal"),
                                    SpriteCell = new Point((int)GoalSize, (int)GoalSize),
                                    Origin = new Vector2(GoalSize / 2, GoalSize),
                                };


                                CurrentGoal.Position = new Vector2((float)Obj.X + CurrentGoal.Origin.X, (float)Obj.Y + CurrentGoal.Origin.Y);
                                CurrentGoal.Bounds.pTopLeft = CurrentGoal.Position - CurrentGoal.Origin;

                                SpritesToAdd.Add(CurrentGoal);
                                break;
                            }

                        case "Lava":
                            {
                                KillBox KB = new KillBox(this, P, Obj);
                                SpritesToAdd.Add(KB);
                                break;
                            }
                        case "Logo":
                            {
                                Sprite Logo = null;
                                if (CommandLineManager.pInstance["lowrezjam"] != null)
                                {
                                    Logo = new Sprite(this)
                                    {
                                        SpriteSheet = Content.Load<Texture2D>("logo_lowrez"),
                                        SpriteCell = new Point(64, 64),
                                        Origin = new Vector2(64 / 2, 64 / 2),
                                        Scale = 1,
                                        Position = new Vector2((float)Obj.X, (float)Obj.Y),
                                    };
                                }
                                else
                                {
                                    Logo = new Sprite(this)
                                    {
                                        SpriteSheet = Content.Load<Texture2D>("logo"),
                                        SpriteCell = new Point(256, 240),
                                        Origin = new Vector2(256 / 2, 240 / 2),
                                        Scale = 0.5f,
                                        Position = new Vector2((float)Obj.X, (float)Obj.Y),
                                    };
                                }

                                SpritesToAdd.Add(Logo);
                                CameraManager.pInstance.pTargetPosition = Logo.Position;

                                break;
                            }

                        case "MainMenu":
                            {
                                SpritesToAdd.Add(new MainMenu(this));
                                break;
                            }

                        case "TriggerFallHard":
                            {
                                SpritesToAdd.Add(new TriggerFallHard(this, P, Obj));
                                break;
                            }

                        case "Bouncer":
                            {
                                Bouncer B = new Bouncer(this, P, Obj)
                                {
                                    SpriteSheet = Content.Load<Texture2D>("powerlight"),
                                    SpriteCell = new Point(8, 8),
                                    Origin = new Vector2(8 / 2, 8 / 2),
                                    Bounds = new Rectangle(8, 8),
                                };
                                B.Animations.Add(Bouncer.Anim_Enabled, new Sprite.Anim(2));
                                B.Animations.Add(Bouncer.Anim_Disabled, new Sprite.Anim(3, 5, 2, false));
                                // Offset by the width and height to make playment a little easier.
                                // Objects with a width/height can be snapped to grid in Tiled.
                                B.Position += new Vector2((float)Obj.X + (float)Obj.Width / 2.0f, (float)Obj.Y + (float)Obj.Height / 2.0f);
                                B.SetNextAnim(Bouncer.Anim_Enabled);
                                SpritesToAdd.Add(B);
                                break;
                            }

                        case "Laser":
                            {
                                Laser L = new Laser(this, P, Obj);
                                SpritesToAdd.Add(L);
                                break;
                            }

                        case "LaserSweeper":
                            {
                                LaserSweeper L = new LaserSweeper(this, P, Obj);
                                SpritesToAdd.Add(L);
                                break;
                            }

                        case "TriggerMusic":
                            {
                                TriggerMusic TM = new TriggerMusic(this, P, Obj);
                                SpritesToAdd.Add(TM);
                                break;
                            }

                        case "Idol":
                            {
                                Idol Idol = new Idol(this, P, Obj)
                                {
                                    SpriteSheet = Content.Load<Texture2D>("battery"),
                                    SpriteCell = new Point(10, 10),
                                    Origin = new Vector2(0, 0),
                                    Bounds = new Rectangle(10, 10),
                                    Position = new Vector2((float)Obj.X, (float)Obj.Y),
                                    Priority = Sprite.PRIORITY_Background,
                                };
                                Idol.Animations.Add("Rotate", new Sprite.Anim(0, 4, 10, true));
                                Idol.Animations.Add("Idle", new Sprite.Anim(0));
                                Idol.SetNextAnim("Idle");
                                SpritesToAdd.Add(Idol);
                                break;
                            }
                    }
                }
            }

            foreach (Sprite S in SpritesToAdd)
            {
                S.FinalizeLoad();
            }


            if (MapName == "EndScreen" || MapName == "EndScreen_LowRez")
            {
                CameraManager.pInstance.pTargetPosition = new Vector2(128 / 2, 120 / 2);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            InputManager.pInstance.UpdateBegin();

#if DEBUG
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                LoadMap("TestMap");
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D2))
            {
                LoadMap("TestMap_Tower");
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D3))
            {
                //LoadMap("Content/TestMap_Ramps.tmx");
            }

            if (Keyboard.GetState().IsKeyDown(Keys.OemPlus))
            {
                CurrentGoal.OnHitGoal();
            }

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad2))
            {
                CameraMoveDir = Vector2.UnitY;
                P.CameraFollowOn = false;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.NumPad8))
            {
                CameraMoveDir = -Vector2.UnitY;
                P.CameraFollowOn = false;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.NumPad6))
            {
                CameraMoveDir = Vector2.UnitX;
                P.CameraFollowOn = false;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.NumPad4))
            {
                CameraMoveDir = -Vector2.UnitX;
                P.CameraFollowOn = false;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.NumPad0))
            {
                CameraMoveDir = Vector2.Zero;
                P.CameraFollowOn = true;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad9))
            {
                CameraMoveSpeed += 0.01f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad7))
            {
                CameraMoveSpeed -= 0.01f;
            }
#endif // DEBUG

            if (!string.IsNullOrEmpty(NextMap))
            {
                LoadMap(NextMap);
                NextMap = null;
            }

            CollisionLayerToggle.Update();
            TileOutlineLayerToggle.Update();
            PostEffectsToggle.Update();

            Sprites.AddRange(SpritesToAdd);
            SpritesToAdd.Clear();

            if (!IsPaused)
            {
                foreach (Sprite S in Sprites)
                {
                    S.Update();
                }
            }

            Sprites.RemoveAll(delegate (Sprite S) { return SpritesToRemove.Contains(S); });
            SpritesToRemove.Clear();

            Sprites = Sprites.OrderBy(x => x.Priority).ToList();
            
            if (CameraMoveDir != Vector2.Zero)
            {
                CameraManager.pInstance.pTargetPosition += CameraMoveDir * CameraMoveSpeed;
            }

            CameraManager.pInstance.Update(gameTime);

            if (InputManager.pInstance.CheckAction(InputManager.InputActions.BACK, true))
            {
                //SetMapToLoad("TitleScreen");
                IsPaused = !IsPaused;
            }

            InputManager.pInstance.UpdateEnd();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Update the elapsed time in the base effect.
            if (PostProcessEffect.Parameters["MSTimeElapsed"] != null)
            {
                PostProcessEffect.Parameters["MSTimeElapsed"].SetValue((float)gameTime.TotalGameTime.TotalSeconds);
            }

            // We will draw the entire game to this render target, and then draw that to the screen.
            GraphicsDevice.SetRenderTarget(PostProcessRenderTarget);

            if (!CollisionLayerToggle.IsEnabled())
            {
                GraphicsDevice.Clear(Color.Black);
            }
            else
            {
                GraphicsDevice.Clear(Color.DarkGray);
            }

            // Draw the game.
            // Immediate mode required to change custom effects on the fly. 
            // NOTE: Seems to make tile tearing worse. (could do only for sprites).
            SpriteBatch.Begin(sortMode: SpriteSortMode.Immediate, samplerState: SamplerState.PointWrap, transformMatrix: CameraManager.pInstance.pFinalTransform);

            int TilsDraw = 0;
            foreach( TmxLayer Layer in mCurrentTileMap.Layers)
            {
                //TmxTileset TileSet = mCurrentTileMap.Tilesets[Layer.Name];

                for (int y = 0; y < mCurrentTileMap.Height; y++)
                {
                    for (int x = 0; x < mCurrentTileMap.Width; x++)
                    {
                        TmxLayerTile Tile = Layer.Tiles[mCurrentTileMap.Width * y + x];
                        int TileID = -1;
                        int TileWidth = -1;
                        int TileHeight = -1;
                        Texture2D Texture = FindTileMapTextureForTileID(Tile.Gid, out TileID, out TileWidth, out TileHeight);
                        if (TileID >= 0)
                        {
                            int atlasX = TileID % (Texture.Width / TileWidth);
                            int atlasY = TileID / (Texture.Width / TileWidth);

                            SpriteEffects FX = SpriteEffects.None;
                            if (Tile.HorizontalFlip)
                            {
                                FX |= SpriteEffects.FlipHorizontally;
                            }
                            if (Tile.VerticalFlip)
                            {
                                FX |= SpriteEffects.FlipVertically;
                            }

                            // For diagonal flips, we need to center the tile and rotate it.
                            float Rotation = 0;
                            Vector2 PosOffset = Vector2.Zero;
                            if (Tile.DiagonalFlip)
                            {
                                // TODO: This render wrong with some combination of FX.
                                Rotation = MathHelper.ToRadians(90);
                                PosOffset = new Vector2(TileSize * 0.5f, TileSize * 0.5f);
                            }

                            Vector2 FinalPos = new Vector2(x * TileSize, y * TileSize) + PosOffset;

                            if (CameraManager.pInstance.IsOnCamera(new Rectangle(8, 8, FinalPos + new Vector2(4,4))))
                            {
                                SpriteBatch.Draw(Texture, FinalPos, new Microsoft.Xna.Framework.Rectangle(atlasX * TileWidth, atlasY * TileHeight, TileWidth, TileHeight), Color.White, Rotation, Vector2.Zero + PosOffset, 1, FX, 0);
                                TilsDraw++;
                            }
                        }
                    }
                }
            }

            //Console.WriteLine("Tiles Drawn: " + TilsDraw);

            // COLLISION TILE MAP
            //
            if (CollisionLayerToggle.IsEnabled() && mCurrentTileMap.Layers.Contains("Collision"))
            {
                TmxLayer Layer = mCurrentTileMap.Layers["Collision"];

                TmxTileset TileSet = mCurrentTileMap.Tilesets["Collision"];

                for (int y = 0; y < mCurrentTileMap.Height; y++)
                {
                    for (int x = 0; x < mCurrentTileMap.Width; x++)
                    {
                        TmxLayerTile Tile = Layer.Tiles[mCurrentTileMap.Width * y + x];
                        int TileID = Tile.Gid - TileSet.FirstGid;
                        if (TileID >= 0)
                        {
                            int atlasX = TileID % (TileSheet.Width / TileSize);
                            int atlasY = TileID / (TileSheet.Width / TileSize);

                            SpriteEffects FX = SpriteEffects.None;
                            if (Tile.HorizontalFlip)
                            {
                                FX = SpriteEffects.FlipHorizontally;
                            }
                            SpriteBatch.Draw(TileSheetCollision, new Vector2(x * TileSize, y * TileSize), new Microsoft.Xna.Framework.Rectangle(atlasX * TileSize, atlasY * TileSize, TileSize, TileSize), Color.White, 0, Vector2.Zero, 1, FX, 0);
                        }
                    }
                }
            }

            // TILE MAP OUTLINE
            //
            if (TileOutlineLayerToggle.IsEnabled() && mCurrentTileMap.Layers.Contains("Gameplay"))
            {
                TmxLayer Layer = mCurrentTileMap.Layers["Gameplay"];

                for (int y = 0; y < mCurrentTileMap.Height; y++)
                {
                    for (int x = 0; x < mCurrentTileMap.Width; x++)
                    {
                        if ((mCurrentTileMap.Width * y + x + y) % 2 == 0)
                        {
                            SpriteBatch.Draw(DebugOutlineTile, new Vector2(x * TileSize, y * TileSize), Color.White * 0.35f);
                        }
                        else
                        {
                            SpriteBatch.Draw(DebugOutlineTile, new Vector2(x * TileSize, y * TileSize), Color.Gray * 0.35f);
                        }
                    }
                }
            }

            // SPRITES
            //
            foreach (Sprite S in Sprites)
            {
                S.Draw(SpriteBatch, gameTime);
            }

            SpriteBatch.End();

            // WHITE NOISE EFFECT
            //
            if (PostEffectsToggle.IsEnabled() && (CommandLineManager.pInstance["lowrezjam"] == null))
            {
                SpriteBatch.Begin(sortMode: SpriteSortMode.Immediate, samplerState: SamplerState.PointWrap, blendState: BlendState.Additive/*, transformMatrix: CameraManager.pInstance.pFinalTransformUI*/);

                int ScreenWidth = Graphics.PreferredBackBufferWidth;
                int ScreenHeight = Graphics.PreferredBackBufferHeight;

                int NeedX = (ScreenWidth / NoiseTexture.Width) + 1;
                int NeedY = (ScreenHeight / NoiseTexture.Height) + 1;

                int U = Rand.Next(NoiseTexture.Width);
                int V = Rand.Next(NoiseTexture.Height);
                int UL = NoiseTexture.Width;
                int VL = NoiseTexture.Height;

                // Give the user some indication that the game is paused.
                float Alpha = 0.3f;
                if (IsPaused)
                {
                    Alpha = 0.7f;
                }

                //float LavaScale = PostProcessEffect.Parameters["LavaColorScale"].GetValueSingle();

                for (int y = 0; y < NeedY; y++)
                {
                    for (int x = 0; x < NeedX; x++)
                    {
                        SpriteBatch.Draw(NoiseTexture, new Vector2(x * NoiseTexture.Width, y * NoiseTexture.Height), new Microsoft.Xna.Framework.Rectangle(U, V, UL, VL), Color.White * Alpha /*MathHelper.SmoothStep(0.3f, 1.0f, LavaScale)*/);
                    }
                }

                SpriteBatch.End();
            }

            // Clear the render target so we can draw the render target to the screen.
            GraphicsDevice.SetRenderTarget(null);

            Effect PPEffect = null;

            if (PostEffectsToggle.IsEnabled())
            {
                PPEffect = PostProcessEffect;
            }

            // Draw the render target as a final pass to allow for post processes.
            SpriteBatch.Begin(sortMode: SpriteSortMode.Immediate, 
                blendState: BlendState.Additive, 
                samplerState: SamplerState.PointClamp, 
                effect: PPEffect);
            SpriteBatch.Draw(PostProcessRenderTarget, Vector2.Zero, Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public int GetTileTypeAtPos(int x, int y, string layerName)
        {
            if (!mCurrentTileMap.Layers.Contains(layerName))
            {
                return EMP;
            }

            int TileX = (int)x / TileSize;
            int TileY = (int)y / TileSize;

            int TileIndex = mCurrentTileMap.Width * TileY + TileX;

            if (TileIndex >= 0 && TileIndex < mCurrentTileMap.Width * mCurrentTileMap.Height)
            {
                return (mCurrentTileMap.Layers[layerName].Tiles[TileIndex].Gid);
            }
            else
            {
                return SOL;
            }
        }

        public int GetTileTypeAtPos(int x, int y, string layerName, ref TmxLayerTile Tile)
        {
            if (!mCurrentTileMap.Layers.Contains(layerName))
            {
                return EMP;
            }

            int TileX = (int)x / TileSize;
            int TileY = (int)y / TileSize;

            int TileIndex = mCurrentTileMap.Width * TileY + TileX;

            if (TileIndex >= 0 && TileIndex < mCurrentTileMap.Width * mCurrentTileMap.Height)
            {
                Tile = mCurrentTileMap.Layers[layerName].Tiles[TileIndex];
                return (Tile.Gid);
            }
            else
            {
                return SOL;
            }
        }

        public bool IsRampRight(int TileType)
        {
            return TileType >= RAMP_RIGHT_START && TileType <= RAMP_RIGHT_END;
        }

        public Texture2D FindTileMapTextureForTileID(int TileID, out int TileIDOut, out int TileWidth, out int TileHeight)
        {
            if (TileID == 0)
            {
                TileIDOut = -1;
                TileWidth = -1;
                TileHeight = -1;
                return null;
            }

            foreach(TmxTileset Set in mCurrentTileMap.Tilesets)
            {
                if (TileID >= Set.FirstGid && TileID < Set.FirstGid + Set.TileCount)
                {
                    TileIDOut = TileID - Set.FirstGid;
                    TileWidth = Set.TileWidth;
                    TileHeight = Set.TileHeight;

                    if (Set.Image.Source == "Content\\tilesheet_collision.png")
                    {
                        return TileSheetCollision;
                    }
                    else if (Set.Image.Source == "Content\\tiles.png")
                    {
                        return TileSheet;
                    }
                    else if (Set.Image.Source == "Content\\thinfont.png")
                    {
                        return ThinFont;
                    }
                    else if (Set.Image.Source == "Content\\thickfont.png")
                    {
                        return ThickFont;
                    }
                    else
                    {
                        throw new Exception("Unknown image source.");
                    }
                }
            }
            throw new Exception("Could not find tile.");
        }

        void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            Window.ClientSizeChanged -= Window_ClientSizeChanged;

            int width = Window.ClientBounds.Width;
            int height = Window.ClientBounds.Height;

            Graphics.PreferredBackBufferWidth = width;
            Graphics.PreferredBackBufferHeight = height;
            Graphics.ApplyChanges();

            // Cannot create a render target of size 0,0.
            if (Graphics.PreferredBackBufferWidth > 0 && Graphics.PreferredBackBufferHeight > 0)
            {
                PostProcessRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight);
            }

            CameraManager.pInstance.OnResolutionChanged(width, height);

            Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        public void OnPlayMusic(bool PlayPunch)
        {
            if (SongFull != null && SongPunch != null)
            {
                if (MediaPlayer.State != MediaState.Playing)
                {
                    if (PlayPunch)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(SongPunch);
                    }
                    else
                    {
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Play(SongFull);
                    }
                }
            }
        }
        
        public void StopAllMusic()
        {
            if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Stop();
            }
        }     
    }
}
