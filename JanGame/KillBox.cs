﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class KillBox : Sprite
    {
        /// <summary>
        /// The only other object the KillBox really cares about.
        /// </summary>
        Mario Player;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="GameIn"></param>
        /// <param name="PlayerIn"></param>
        /// <param name="ObjIn"></param>
        public KillBox(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) 
            : base(GameIn)
        {
            Player = PlayerIn;

            // Create the bounds bases on what was specified in the map.
            Bounds = new Rectangle((int)ObjIn.Width, (int)ObjIn.Height);
            
            Position = new Vector2((float)ObjIn.X, (float)ObjIn.Y);
        }

        /// <summary>
        /// See parent.
        /// </summary>
        public override void Update()
        {
            // Note: We check the players position (aka his feet) instead of the bounding box to make it more forgiving.
            //       may want to change this if we get more customizable bounding boxes.
            if (Bounds.Intersects(Player.Position))
            {
                // Tell the player they have entered lava.
                Player.OnEnterLava();
            }

            base.Update();
        }
    }
}
