﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class TriggerMusic : Trigger
    {
        bool ShortSong = true;

        public TriggerMusic(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn, PlayerIn, ObjIn)
        {
            if (ObjIn.Properties.ContainsKey("PlayFullSong"))
            {
                ShortSong = false;
            }
        }
        public override void OnTriggered()
        {
            Game.OnPlayMusic(ShortSong);
            IsEnabled = false;
        }
    }
}
