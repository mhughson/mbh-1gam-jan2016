sampler TextureSampler : register(s0);
float MSTimeElapsed = 0;

float4 Tint = float4(0.5,0.5,0.5,1);
float Intensity = 0.45;
float PulseRate = 3;

float4 main(float4 position : SV_Position, float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
	// Look up the texture color.
	float4 tex = tex2D(TextureSampler, texCoord);

	// The final tint value uses the color of the tint, but maintains the alpha of
	// the texture since we don't want to color transparent pixels.
	float4 mergedTint = float4(Tint.r, Tint.g, Tint.b, tex.a);

	// Blend from the texture color to the full on tint based on the alpha value.
	//tex = lerp(tex, mergedTint, Tint.a) * tex.a;

	tex.rgb = lerp(tex, Tint.rgb, smoothstep(-1, 1, sin(MSTimeElapsed * PulseRate)) * Intensity) * tex.a;

	return tex;
}


technique Technique1
{
	pass Pass1
	{
		PixelShader = compile ps_4_0 main();
	}
}