﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;

namespace JanGame
{
    class TriggerFallHard : Trigger
    {
        public TriggerFallHard(Game1 GameIn, Mario PlayerIn, TmxObject ObjIn) : base(GameIn, PlayerIn, ObjIn)
        {

        }
        public override void OnTriggered()
        {
            Player.FallingHard = true;
            IsEnabled = false;
        }
    }
}
