﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JanGame
{
    class MainMenu : Sprite
    {
        public MainMenu(Game1 GameIn) : base(GameIn)
        {
            Game.StopAllMusic();
        }

        public override void Update()
        {
            if (InputManager.pInstance.CheckAction(InputManager.InputActions.A, true))
            {

                float TicksForLavaRespawn = 120;

                float InstantFlashTicks = 15;

                Action StartLoad = () =>
                {

                    Game.SetMapToLoad("1_1");
                };

                Action Act = () => {
                    PostProcessScalarTween Tween = new PostProcessScalarTween(Game, 0, 1,
                        TicksForLavaRespawn * 0.75f - InstantFlashTicks, "LavaColorScale", Game.PostProcessEffect, StartLoad);
                    Game.SpritesToAdd.Add(Tween);
                };

                PostProcessScalarTween PP = new PostProcessScalarTween(Game, 1, 0,
                    InstantFlashTicks, "LavaColorScale", Game.PostProcessEffect, Act);
                Game.SpritesToAdd.Add(PP);
            }
            else if (InputManager.pInstance.CheckAction(InputManager.InputActions.BACK, true))
            {
                // TODO: This is getting hit on the same BACK button press that leaves the level.
                Game.Exit();
            }

            base.Update();
        }
    }
}
